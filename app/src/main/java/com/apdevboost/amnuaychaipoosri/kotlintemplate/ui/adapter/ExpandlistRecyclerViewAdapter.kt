package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.adapter

import android.content.Context
import android.support.v7.recyclerview.extensions.AsyncDifferConfig
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ViewholderEmptyBinding
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ViewholderExpandlistHeaderItemBinding
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ViewholderExpandlistItemBinding
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient
import com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.callback.DrinkIngredientDiffCallback
import com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.EmptyViewHolder

open class ExpandlistRecyclerViewAdapter : ListAdapter<DrinkIngredient, RecyclerView.ViewHolder>(AsyncDifferConfig.Builder(DrinkIngredientDiffCallback()).build()) {
    private val VIEWTYPE_HEADER: Int = 1
    private val VIEWTYPE_ITEM: Int = 2
    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when(viewType) {
            VIEWTYPE_HEADER -> {
                val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
                context = parent.context
                val viewholderExpandlistHeaderItemBinding: ViewholderExpandlistHeaderItemBinding = ViewholderExpandlistHeaderItemBinding.inflate(layoutInflater, parent, false)
                return ExpandlistHeaderViewItemHolder(viewholderExpandlistHeaderItemBinding)

            }
            VIEWTYPE_ITEM -> {
                val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
                context = parent.context
                val viewholderExpandlistItemBinding: ViewholderExpandlistItemBinding = ViewholderExpandlistItemBinding.inflate(layoutInflater, parent, false)
                return ExpandlistViewItemHolder(viewholderExpandlistItemBinding)

            }
            else -> {
                val layoutInflater:LayoutInflater = LayoutInflater.from(parent.context)
                val viewholderEmptyBinding: ViewholderEmptyBinding = ViewholderEmptyBinding.inflate(layoutInflater, parent, false)
                return EmptyViewHolder(viewholderEmptyBinding)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when(position) {
            0 -> VIEWTYPE_HEADER
            else -> VIEWTYPE_ITEM
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount()+1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ExpandlistHeaderViewItemHolder) {
            holder.bind()

        }
        else if(holder is ExpandlistViewItemHolder)
        {
            val drinkIngredient: DrinkIngredient = getItem(position-1)
            holder.binding.viewholderExpandlistItemNumberTV.text = ""+position
            holder.binding.viewholderExpandlistItemNameTV.text = drinkIngredient.drinkIngredientName
            holder.binding.viewholderExpandlistItemQuantityTV.text = drinkIngredient.drinkIngredientWeight
        }
    }


    inner class ExpandlistHeaderViewItemHolder(val binding: ViewholderExpandlistHeaderItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            binding.executePendingBindings()
        }
    }

    inner class ExpandlistViewItemHolder(val binding: ViewholderExpandlistItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            binding.executePendingBindings()
        }
    }

}