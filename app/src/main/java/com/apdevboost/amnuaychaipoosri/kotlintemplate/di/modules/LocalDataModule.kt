package com.apdevboost.amnuaychaipoosri.kotlintemplate.modules

import android.arch.persistence.room.Room
import android.content.Context
import android.content.SharedPreferences
import com.apdevboost.amnuaychaipoosri.kotlintemplate.constant.Constants
import com.apdevboost.amnuaychaipoosri.kotlintemplate.local.AppDatabase
import com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkDao
import com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkIngredientDao
import com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager
import com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class LocalDataModule(val context: Context) {

    @Provides
    @ApplicationScope
    fun provideDrinkDao(db: AppDatabase): DrinkDao = db.drinkDao()

    @Provides
    @ApplicationScope
    fun provideDrinkIngredientDao(db: AppDatabase): DrinkIngredientDao = db.drinkIngredientDao()

    @Provides
    @ApplicationScope
    fun provideAppDatabase(): AppDatabase =
            Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, Constants.DB_FILE_NAME)
                    .build()

    //Share Preference
    @Provides
    @ApplicationScope
    fun provideSharedPreferences(): SharedPreferences = context.applicationContext.getSharedPreferences(Constants.SHARE_PREF_NAME, Context.MODE_PRIVATE)

    @Provides
    @ApplicationScope
    fun provideSharedPreferencesEditor(pref: SharedPreferences): SharedPreferences.Editor = pref.edit()

    @Provides
    @ApplicationScope
    fun provideUserDataManager(pref: SharedPreferences, editor: SharedPreferences.Editor): UserDataManager = UserDataManager(pref, editor)

}
