package com.apdevboost.amnuaychaipoosri.kotlintemplate.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkIngredient

@Dao
interface DrinkIngredientDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdateDrinkIngredientList(vararg drinkIngredientList: DrinkIngredient)

    @Query("SELECT * FROM drink_ingredient WHERE id = (:drinkId)")
    fun getDrinkIngredientList(drinkId: Long?): LiveData<List<DrinkIngredient>>


//    @Query("SELECT * FROM drink_ingredient where drinkId = :drinkId")
//    fun getDrinkIngredientListByDrinkId(drinkId: Int): List<DrinkIngredient>

}