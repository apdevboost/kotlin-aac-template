package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.custom.dialog

import android.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apdevboost.amnuaychaipoosri.kotlintemplate.R
import com.apdevboost.amnuaychaipoosri.kotlintemplate.custom.base.BaseDialog
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.DialogSingleButtonBinding

class SingleButtonDialog : BaseDialog() {

    private lateinit var binding: DialogSingleButtonBinding
    private var dialogClickListener: DialogSingleButtonClickListener? = null
    private var title:String? = null
    private var subTitle:String? = null
    private var buttonName:String? = null
    private var request:Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.ThemeDialog)
        if (savedInstanceState == null) {
            onRestorenonInstanceState(arguments)
        } else {
            onRestoreInstanceState(savedInstanceState)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<DialogSingleButtonBinding>(inflater, R.layout.dialog_single_button, container, false).also {
            binding = it
        }.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        initial()
    }

    private fun initial() {
        binding.tvTitle.text = title
        binding.tvSubTitle.text = subTitle
        binding.btnOK.text = buttonName

        dialogClickListener = if (parentFragment != null) {
            parentFragment as DialogSingleButtonClickListener
        } else {
            activity as DialogSingleButtonClickListener
        }

        binding.btnOK?.setOnClickListener({ view ->
            dialogClickListener?.onDialogSingleButtonClickListener(this, view, request)
        })

    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("title", title)
        outState.putString("subTitle", subTitle)
        outState.putString("buttonName", buttonName)
        outState.putInt("request", request)
        super.onSaveInstanceState(outState)
    }

    private fun onRestoreInstanceState(saveInstanceState: Bundle?) {
        title = saveInstanceState?.getString("title")
        subTitle = saveInstanceState?.getString("subTitle")
        buttonName = saveInstanceState?.getString("buttonName")
        request = saveInstanceState?.getInt("request", 0)!!
    }

    private fun onRestorenonInstanceState(bundle: Bundle?) {
        title = bundle?.getString("title")
        subTitle = bundle?.getString("subTitle")
        buttonName = bundle?.getString("buttonName")
        request = bundle?.getInt("request", 0)!!
    }

    companion object {
        fun newInstance(title: String?, subTitle: String?, buttonName: String?, request: Int): SingleButtonDialog {
            val fragmentDialog = SingleButtonDialog()
            val bundle = Bundle()
            bundle.putString("title", title)
            bundle.putString("subTitle", subTitle)
            bundle.putString("buttonName", buttonName)
            bundle.putInt("request", request)
            fragmentDialog.arguments = bundle
            return fragmentDialog
        }
    }

    open class Builder {

        private var title: String? = null
        private var subTitle: String? = null
        private var buttonName: String? = null
        private var request: Int = 0
        private var dialogClickListener: DialogSingleButtonClickListener? = null

        fun build(): SingleButtonDialog {
            return SingleButtonDialog.newInstance(title, subTitle, buttonName, request)
        }

        fun setTitle(title: String): Builder {
            this.title = title
            return this
        }

        fun setSubTitle(subTitle: String): Builder {
            this.subTitle = subTitle

            return this
        }
        fun setbuttonName(buttonName: String): Builder {
            this.buttonName = buttonName
            return this
        }

        fun request(request: Int): Builder {
            this.request = request
            return this
        }

        fun setOnButtonClickListener(dialogSingleButtonClickListener: DialogSingleButtonClickListener): Builder {
            this.dialogClickListener = dialogSingleButtonClickListener
            return this
        }
    }

    interface DialogSingleButtonClickListener {
        fun onDialogSingleButtonClickListener(dialog: SingleButtonDialog, v: View?, request: Int)
    }

}