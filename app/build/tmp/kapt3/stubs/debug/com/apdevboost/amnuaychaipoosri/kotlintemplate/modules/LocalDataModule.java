package com.apdevboost.amnuaychaipoosri.kotlintemplate.modules;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\bH\u0007J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\bH\u0007J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000b\u001a\u00020\bH\u0007J\b\u0010\u000e\u001a\u00020\u000fH\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u000fH\u0007J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\u0011H\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0016"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/modules/LocalDataModule;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getContext", "()Landroid/content/Context;", "provideAppDatabase", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/AppDatabase;", "provideDrinkDao", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkDao;", "db", "provideDrinkIngredientDao", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/local/DrinkIngredientDao;", "provideSharedPreferences", "Landroid/content/SharedPreferences;", "provideSharedPreferencesEditor", "Landroid/content/SharedPreferences$Editor;", "pref", "provideUserDataManager", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;", "editor", "app_debug"})
@dagger.Module()
public final class LocalDataModule {
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkDao provideDrinkDao(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.local.AppDatabase db) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkIngredientDao provideDrinkIngredientDao(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.local.AppDatabase db) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.local.AppDatabase provideAppDatabase() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final android.content.SharedPreferences provideSharedPreferences() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final android.content.SharedPreferences.Editor provideSharedPreferencesEditor(@org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences pref) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope()
    @dagger.Provides()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager provideUserDataManager(@org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences pref, @org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences.Editor editor) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public LocalDataModule(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}