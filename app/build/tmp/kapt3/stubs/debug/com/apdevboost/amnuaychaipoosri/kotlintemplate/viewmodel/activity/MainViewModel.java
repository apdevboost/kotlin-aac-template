package com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u000f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00120\u00110\u0010J\u0006\u0010\u0014\u001a\u00020\u0015J\u0006\u0010\u0016\u001a\u00020\u0017R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\n8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u0018"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/viewmodel/activity/MainViewModel;", "Landroid/arch/lifecycle/ViewModel;", "()V", "drinkRepo", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/drink/DrinkRepository;", "getDrinkRepo", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/drink/DrinkRepository;", "setDrinkRepo", "(Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/repository/drink/DrinkRepository;)V", "userDataManager", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;", "getUserDataManager", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;", "setUserDataManager", "(Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;)V", "getDrinkListData", "Landroid/arch/lifecycle/LiveData;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/Resource;", "", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/Drink;", "isUserLogin", "", "logout", "", "app_debug"})
public final class MainViewModel extends android.arch.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink.DrinkRepository drinkRepo;
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager userDataManager;
    
    @org.jetbrains.annotations.NotNull()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink.DrinkRepository getDrinkRepo() {
        return null;
    }
    
    public final void setDrinkRepo(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink.DrinkRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager getUserDataManager() {
        return null;
    }
    
    public final void setUserDataManager(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.arch.lifecycle.LiveData<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.Resource<java.util.List<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.Drink>>> getDrinkListData() {
        return null;
    }
    
    public final boolean isUserLogin() {
        return false;
    }
    
    public final void logout() {
    }
    
    public MainViewModel() {
        super();
    }
}