package com.apdevboost.amnuaychaipoosri.kotlintemplate.manager;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\t\u001a\u00020\bJ\u0006\u0010\n\u001a\u00020\u000bJ\u0010\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\bR\u000e\u0010\u0007\u001a\u00020\bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/manager/UserDataManager;", "", "pref", "Landroid/content/SharedPreferences;", "editor", "Landroid/content/SharedPreferences$Editor;", "(Landroid/content/SharedPreferences;Landroid/content/SharedPreferences$Editor;)V", "KEY_USER_TOKEN", "", "getUserToken", "isUserLogin", "", "setUserToken", "", "token", "app_debug"})
@javax.inject.Singleton()
public final class UserDataManager {
    private final java.lang.String KEY_USER_TOKEN = "user_token";
    private final android.content.SharedPreferences pref = null;
    private final android.content.SharedPreferences.Editor editor = null;
    
    public final void setUserToken(@org.jetbrains.annotations.Nullable()
    java.lang.String token) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUserToken() {
        return null;
    }
    
    public final boolean isUserLogin() {
        return false;
    }
    
    public UserDataManager(@org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences pref, @org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences.Editor editor) {
        super();
    }
}