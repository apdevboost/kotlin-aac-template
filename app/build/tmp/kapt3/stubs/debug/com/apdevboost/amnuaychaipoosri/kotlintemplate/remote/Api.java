package com.apdevboost.amnuaychaipoosri.kotlintemplate.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00040\u0003H\'\u00a8\u0006\u0007"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/remote/Api;", "", "getDrinkList", "Landroid/arch/lifecycle/LiveData;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/ApiResponse;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/base/BaseResponse;", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/model/drink/DrinkListResult;", "app_debug"})
public abstract interface Api {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "v2/5ae5887b2e000063003824b9")
    public abstract android.arch.lifecycle.LiveData<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.ApiResponse<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.BaseResponse<com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkListResult>>> getDrinkList();
}