// Generated by Dagger (https://google.github.io/dagger).
package com.apdevboost.amnuaychaipoosri.kotlintemplate.component;

import android.app.Application;
import android.content.SharedPreferences;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.local.AppDatabase;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkDao;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.local.DrinkIngredientDao;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.AppExecutorsModule;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.AppExecutorsModule_ProvideAppExecutorsFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.AppModule;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.ConfigurationModule;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.LocalDataModule;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.LocalDataModule_ProvideAppDatabaseFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.LocalDataModule_ProvideDrinkDaoFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.LocalDataModule_ProvideDrinkIngredientDaoFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.LocalDataModule_ProvideSharedPreferencesEditorFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.LocalDataModule_ProvideSharedPreferencesFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.LocalDataModule_ProvideUserDataManagerFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.RemoteDataModule;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.RemoteDataModule_ProvideApiFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.RemoteDataModule_ProvideLoginApiFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.RemoteDataModule_ProvideRetrofitFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.RepositoryModule;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.RepositoryModule_DrinkRepositoryImplFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.modules.RepositoryModule_ProvideLoginRepositoryFactory;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.Api;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.remote.LoginApi;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.base.AppExecutors;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.drink.DrinkRepository;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.repository.login.LoginRepository;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.template.viewmodel.TemplateViewModel;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.MainViewModel;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.MainViewModel_MembersInjector;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.SplashViewModel;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.SplashViewModel_MembersInjector;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.fragment.LoginFragmentViewModel;
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.fragment.LoginFragmentViewModel_MembersInjector;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.inject.Provider;
import retrofit2.Retrofit;

public final class DaggerAppComponent implements AppComponent {
  private Provider<SharedPreferences> provideSharedPreferencesProvider;

  private Provider<SharedPreferences.Editor> provideSharedPreferencesEditorProvider;

  private Provider<UserDataManager> provideUserDataManagerProvider;

  private Provider<AppDatabase> provideAppDatabaseProvider;

  private Provider<DrinkDao> provideDrinkDaoProvider;

  private Provider<DrinkIngredientDao> provideDrinkIngredientDaoProvider;

  private Provider<Retrofit> provideRetrofitProvider;

  private Provider<Api> provideApiProvider;

  private Provider<AppExecutors> provideAppExecutorsProvider;

  private Provider<DrinkRepository> drinkRepositoryImplProvider;

  private Provider<LoginApi> provideLoginApiProvider;

  private Provider<LoginRepository> provideLoginRepositoryProvider;

  private DaggerAppComponent(Builder builder) {
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {
    this.provideSharedPreferencesProvider =
        DoubleCheck.provider(
            LocalDataModule_ProvideSharedPreferencesFactory.create(builder.localDataModule));
    this.provideSharedPreferencesEditorProvider =
        DoubleCheck.provider(
            LocalDataModule_ProvideSharedPreferencesEditorFactory.create(
                builder.localDataModule, provideSharedPreferencesProvider));
    this.provideUserDataManagerProvider =
        DoubleCheck.provider(
            LocalDataModule_ProvideUserDataManagerFactory.create(
                builder.localDataModule,
                provideSharedPreferencesProvider,
                provideSharedPreferencesEditorProvider));
    this.provideAppDatabaseProvider =
        DoubleCheck.provider(
            LocalDataModule_ProvideAppDatabaseFactory.create(builder.localDataModule));
    this.provideDrinkDaoProvider =
        DoubleCheck.provider(
            LocalDataModule_ProvideDrinkDaoFactory.create(
                builder.localDataModule, provideAppDatabaseProvider));
    this.provideDrinkIngredientDaoProvider =
        DoubleCheck.provider(
            LocalDataModule_ProvideDrinkIngredientDaoFactory.create(
                builder.localDataModule, provideAppDatabaseProvider));
    this.provideRetrofitProvider =
        DoubleCheck.provider(
            RemoteDataModule_ProvideRetrofitFactory.create(builder.remoteDataModule));
    this.provideApiProvider =
        DoubleCheck.provider(
            RemoteDataModule_ProvideApiFactory.create(
                builder.remoteDataModule, provideRetrofitProvider));
    this.provideAppExecutorsProvider =
        DoubleCheck.provider(
            AppExecutorsModule_ProvideAppExecutorsFactory.create(builder.appExecutorsModule));
    this.drinkRepositoryImplProvider =
        DoubleCheck.provider(
            RepositoryModule_DrinkRepositoryImplFactory.create(
                builder.repositoryModule,
                provideDrinkDaoProvider,
                provideDrinkIngredientDaoProvider,
                provideApiProvider,
                provideAppExecutorsProvider));
    this.provideLoginApiProvider =
        DoubleCheck.provider(
            RemoteDataModule_ProvideLoginApiFactory.create(
                builder.remoteDataModule, provideRetrofitProvider));
    this.provideLoginRepositoryProvider =
        DoubleCheck.provider(
            RepositoryModule_ProvideLoginRepositoryFactory.create(
                builder.repositoryModule, provideLoginApiProvider, provideUserDataManagerProvider));
  }

  @Override
  public void inject(Application application) {}

  @Override
  public void inject(SplashViewModel splashViewModel) {
    injectSplashViewModel(splashViewModel);
  }

  @Override
  public void inject(MainViewModel mainViewModel) {
    injectMainViewModel(mainViewModel);
  }

  @Override
  public void inject(TemplateViewModel templateViewModel) {}

  @Override
  public void inject(LoginFragmentViewModel loginFragmentViewModel) {
    injectLoginFragmentViewModel(loginFragmentViewModel);
  }

  private SplashViewModel injectSplashViewModel(SplashViewModel instance) {
    SplashViewModel_MembersInjector.injectUserDataManager(
        instance, provideUserDataManagerProvider.get());
    return instance;
  }

  private MainViewModel injectMainViewModel(MainViewModel instance) {
    MainViewModel_MembersInjector.injectDrinkRepo(instance, drinkRepositoryImplProvider.get());
    MainViewModel_MembersInjector.injectUserDataManager(
        instance, provideUserDataManagerProvider.get());
    return instance;
  }

  private LoginFragmentViewModel injectLoginFragmentViewModel(LoginFragmentViewModel instance) {
    LoginFragmentViewModel_MembersInjector.injectLoginRepository(
        instance, provideLoginRepositoryProvider.get());
    return instance;
  }

  public static final class Builder {
    private LocalDataModule localDataModule;

    private RemoteDataModule remoteDataModule;

    private AppExecutorsModule appExecutorsModule;

    private RepositoryModule repositoryModule;

    private Builder() {}

    public AppComponent build() {
      if (localDataModule == null) {
        throw new IllegalStateException(LocalDataModule.class.getCanonicalName() + " must be set");
      }
      if (remoteDataModule == null) {
        throw new IllegalStateException(RemoteDataModule.class.getCanonicalName() + " must be set");
      }
      if (appExecutorsModule == null) {
        this.appExecutorsModule = new AppExecutorsModule();
      }
      if (repositoryModule == null) {
        this.repositoryModule = new RepositoryModule();
      }
      return new DaggerAppComponent(this);
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This
     *     method is a no-op. For more, see https://google.github.io/dagger/unused-modules.
     */
    @Deprecated
    public Builder appModule(AppModule appModule) {
      Preconditions.checkNotNull(appModule);
      return this;
    }

    public Builder localDataModule(LocalDataModule localDataModule) {
      this.localDataModule = Preconditions.checkNotNull(localDataModule);
      return this;
    }

    public Builder remoteDataModule(RemoteDataModule remoteDataModule) {
      this.remoteDataModule = Preconditions.checkNotNull(remoteDataModule);
      return this;
    }

    public Builder appExecutorsModule(AppExecutorsModule appExecutorsModule) {
      this.appExecutorsModule = Preconditions.checkNotNull(appExecutorsModule);
      return this;
    }

    public Builder repositoryModule(RepositoryModule repositoryModule) {
      this.repositoryModule = Preconditions.checkNotNull(repositoryModule);
      return this;
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This
     *     method is a no-op. For more, see https://google.github.io/dagger/unused-modules.
     */
    @Deprecated
    public Builder configurationModule(ConfigurationModule configurationModule) {
      Preconditions.checkNotNull(configurationModule);
      return this;
    }
  }
}
