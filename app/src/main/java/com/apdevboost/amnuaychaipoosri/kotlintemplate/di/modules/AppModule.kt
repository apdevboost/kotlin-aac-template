package com.apdevboost.amnuaychaipoosri.kotlintemplate.modules

import android.app.Application
import android.content.Context
import com.apdevboost.amnuaychaipoosri.kotlintemplate.scopes.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class AppModule(val application: Application) {

    @Provides
    @ApplicationScope
    fun provideApplication() : Application {
        return application
    }

    @Provides
    @ApplicationScope
    fun provideContext() : Context {
        return application
    }

}