package com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity

import android.arch.lifecycle.ViewModel
import com.apdevboost.amnuaychaipoosri.kotlintemplate.manager.UserDataManager
import javax.inject.Inject

class SplashViewModel : ViewModel() {

    @Inject
    lateinit var userDataManager: UserDataManager

    fun isUserLogin(): Boolean {
        return userDataManager.isUserLogin()
    }

}