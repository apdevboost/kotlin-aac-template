package com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import com.apdevboost.amnuaychaipoosri.kotlintemplate.MainApplication
import com.apdevboost.amnuaychaipoosri.kotlintemplate.R
import com.apdevboost.amnuaychaipoosri.kotlintemplate.databinding.ActivitySplashBinding
import com.apdevboost.amnuaychaipoosri.kotlintemplate.ui.activity.base.BaseActivity
import com.apdevboost.amnuaychaipoosri.kotlintemplate.viewmodel.activity.SplashViewModel

class SplashActivity : BaseActivity() {

    private val TAG = "SplashActivity"
    private val handler: Handler = Handler()

    private val splashViewModel: SplashViewModel by lazy {
        ViewModelProviders.of(this).get(SplashViewModel::class.java).also {
            MainApplication.appComponent.inject(it)
        }
    }

    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        initial()
    }

    private fun initial() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        handler.postDelayed(Runnable {
            proceedToNextPage()
        }, 2000)
    }

    private fun proceedToNextPage() {
        if (splashViewModel.isUserLogin()) {
            goToMainPage()
        } else {
            goToLoginPage()
        }
    }

    private fun goToMainPage()
    {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun goToLoginPage()
    {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}