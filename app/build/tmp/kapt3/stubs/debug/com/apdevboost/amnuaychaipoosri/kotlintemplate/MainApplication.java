package com.apdevboost.amnuaychaipoosri.kotlintemplate;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \b2\u00020\u0001:\u0001\bB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0014J\b\u0010\u0007\u001a\u00020\u0004H\u0016\u00a8\u0006\t"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/MainApplication;", "Landroid/support/multidex/MultiDexApplication;", "()V", "attachBaseContext", "", "base", "Landroid/content/Context;", "onCreate", "Companion", "app_debug"})
public final class MainApplication extends android.support.multidex.MultiDexApplication {
    @org.jetbrains.annotations.NotNull()
    public static com.apdevboost.amnuaychaipoosri.kotlintemplate.component.AppComponent appComponent;
    public static final com.apdevboost.amnuaychaipoosri.kotlintemplate.MainApplication.Companion Companion = null;
    
    @java.lang.Override()
    protected void attachBaseContext(@org.jetbrains.annotations.Nullable()
    android.content.Context base) {
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    public MainApplication() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.apdevboost.amnuaychaipoosri.kotlintemplate.component.AppComponent getAppComponent() {
        return null;
    }
    
    public static final void setAppComponent(@org.jetbrains.annotations.NotNull()
    com.apdevboost.amnuaychaipoosri.kotlintemplate.component.AppComponent p0) {
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R$\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\u0005\u0010\u0002\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\n"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/MainApplication$Companion;", "", "()V", "appComponent", "Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/component/AppComponent;", "appComponent$annotations", "getAppComponent", "()Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/component/AppComponent;", "setAppComponent", "(Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/component/AppComponent;)V", "app_debug"})
    public static final class Companion {
        
        public static void appComponent$annotations() {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.apdevboost.amnuaychaipoosri.kotlintemplate.component.AppComponent getAppComponent() {
            return null;
        }
        
        public final void setAppComponent(@org.jetbrains.annotations.NotNull()
        com.apdevboost.amnuaychaipoosri.kotlintemplate.component.AppComponent p0) {
        }
        
        private Companion() {
            super();
        }
    }
}