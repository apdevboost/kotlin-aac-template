package com.apdevboost.amnuaychaipoosri.kotlintemplate.custom.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/custom/base/BaseDialog;", "Landroid/support/v4/app/DialogFragment;", "()V", "app_debug"})
public class BaseDialog extends android.support.v4.app.DialogFragment {
    private java.util.HashMap _$_findViewCache;
    
    public BaseDialog() {
        super();
    }
}