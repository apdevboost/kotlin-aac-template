package com.apdevboost.amnuaychaipoosri.kotlintemplate.template.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/apdevboost/amnuaychaipoosri/kotlintemplate/template/viewmodel/TemplateViewModel;", "Landroid/arch/lifecycle/ViewModel;", "()V", "app_debug"})
public final class TemplateViewModel extends android.arch.lifecycle.ViewModel {
    
    public TemplateViewModel() {
        super();
    }
}