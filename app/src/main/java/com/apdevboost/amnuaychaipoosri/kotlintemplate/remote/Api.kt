package com.apdevboost.amnuaychaipoosri.kotlintemplate.remote;

import android.arch.lifecycle.LiveData
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.ApiResponse
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.base.BaseResponse
import com.apdevboost.amnuaychaipoosri.kotlintemplate.model.drink.DrinkListResult
import retrofit2.http.GET

interface Api {

    @GET("v2/5ae5887b2e000063003824b9")
    fun getDrinkList(): LiveData<ApiResponse<BaseResponse<DrinkListResult>>>
}